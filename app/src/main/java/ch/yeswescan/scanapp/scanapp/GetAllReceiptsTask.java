package ch.yeswescan.scanapp.scanapp;

import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.joda.time.DateTime;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.List;

/**
 * Created by timethy on 10/11/14
 */
class GetAllReceiptsTask extends AsyncTask<Integer, Void, List<Receipt>> {
	private static final HttpClient HTTP_CLIENT = AndroidHttpClient.newInstance("ladyfly");

    // Get JSON from REST
    InputStreamReader rd;
    OutputStreamWriter wr;
    protected List<Receipt> doInBackground(Integer... ID) {
        try  {
			URI uri = new URI("http://yws.voodle.de:8081/user/" + ID[0] + "/all-receipts");
            Log.i("URI I'M REQUESTING", uri.toString());
			HttpResponse response = HTTP_CLIENT.execute(new HttpGet(uri));
			HttpEntity entity = response.getEntity();
            // Get the response
            rd = new InputStreamReader(entity.getContent());
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Parse JSON with gson

        JsonDeserializer<DateTime> deser = new JsonDeserializer<DateTime>() {
            @Override
            public DateTime deserialize(JsonElement json, Type typeOfT,
                                    JsonDeserializationContext context) {
                return json == null ? null : new DateTime(json.getAsLong());
            }
        };

        final Gson gson = new GsonBuilder().registerTypeAdapter(DateTime.class, deser).create();
        return gson.fromJson(rd, new TypeToken<List<Receipt>>(){}.getType());
    }
}
