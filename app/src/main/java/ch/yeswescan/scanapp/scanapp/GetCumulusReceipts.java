package ch.yeswescan.scanapp.scanapp;

import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.joda.time.DateTime;

import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by timethy on 10/11/14
 */
public class GetCumulusReceipts extends AsyncTask<Integer, Void, List<Receipt>> {
	private static final HttpClient HTTP_CLIENT = AndroidHttpClient.newInstance("ladyfly");

	protected List<Receipt> doInBackground(Integer... ids) {
		int userId = ids[0];
		int privateGroupId = ids[1];
		int cumulusId = ids[2];
        try  {
			URI uri = new URI("http://api.autoidlabs.ch/pos/" + cumulusId);
            Log.i("URI I'M REQUESTING", uri.toString());
			HttpResponse response = HTTP_CLIENT.execute(new HttpGet(uri));
			HttpEntity entity = response.getEntity();
            // Get the response
            InputStreamReader rd = new InputStreamReader(entity.getContent());
			JsonElement json = new JsonParser().parse(rd);
			Map<Integer, Receipt> receiptMap = new TreeMap<Integer, Receipt>();
			for(JsonElement elem: json.getAsJsonArray()) {
				JsonObject jsonItem = elem.getAsJsonObject();
				int cumulusReceiptId = jsonItem.get("receiptId").getAsInt();
				long ean = jsonItem.get("migrosEan").getAsLong();
				DateTime createDate = new DateTime(jsonItem.get("rDate").getAsString());
				double total = jsonItem.get("price").getAsDouble();
				Receipt r = receiptMap.get(cumulusReceiptId);
				if(r == null) {
					r = new Receipt();
					r.createDate = createDate;
					r.store = "Cumulus";
					r.mode = "Cumulus";
					r.creator = userId;
					r.total = 0.0;
					r.items = new ArrayList<Item>();
					r.groups = new TreeMap<Integer, Double>();
					// WARNING: DIRTY HACK FOR CUMULUS ID, they are just negative!
					r.receiptId = -cumulusReceiptId;
					r.sourceReceiptId = cumulusReceiptId;
					receiptMap.put(cumulusReceiptId, r);
				}
				r.total += total;
				Item i = new Item();
				/*{
					URI productURI = new URI("http://api.autoidlabs.ch/products/" + ean);
					HttpResponse productResponse = HTTP_CLIENT.execute(new HttpGet(productURI));
					HttpEntity productEntity = productResponse.getEntity();
					// Get the response
					JsonElement productJson = new JsonParser().parse(new InputStreamReader(productEntity.getContent()));
					JsonElement nameJson = productJson.getAsJsonObject().get("name");
					i.itemName = nameJson == null ? Long.toString(ean) : nameJson.getAsString();
				}*/
				i.itemName = Long.toString(ean);
				i.receiptId = -cumulusReceiptId;
				i.groupId = privateGroupId;
				i.deleted = false;
				i.itemNr = r.items.size();
				i.quantity = jsonItem.get("quantNorm").getAsDouble();
				i.itemPrice = jsonItem.get("pricePerUnit").getAsDouble();
				r.items.add(i);
				r.groups.put(privateGroupId, r.total);
			}
			return new ArrayList<Receipt>(receiptMap.values());
        } catch (Exception e) {
            e.printStackTrace();
        }
		return null;
	}
}
