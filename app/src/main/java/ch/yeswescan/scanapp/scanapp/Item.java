package ch.yeswescan.scanapp.scanapp;

/**
 * Created by kklein on 11/10/14.
 */
public class Item {
    Integer receiptId;
    Integer itemNr;
    String itemName;
    Double quantity;
    Double itemPrice;
    Integer groupId;
    boolean deleted;
}
