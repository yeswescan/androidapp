package ch.yeswescan.scanapp.scanapp;

import android.app.Activity;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.*;

public class OverviewActivity extends Activity implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private VisibilityFilterArrayAdapter adapter;

    protected static Map<Integer, Receipt> receipts = new TreeMap<Integer, Receipt>();
	protected static Map<Integer, Group> groups = new TreeMap<Integer, Group>();
    protected static Map<Integer, Integer> groupColorCode = new HashMap<Integer, Integer>();
	protected static List<Group> groupList = new ArrayList<Group>();

	private String getGroupName(int i) {
		if(i == 0) {
			return "All";
		} else if(i == groupList.size() + 1) {
			return "Cumulus";
		} else {
			return groupList.get(i-1).groupName;
		}
	}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview);

        mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

        int userId = LoginActivity.user.userId;
		int privateGroupId = 0;

        try {
            groupList = new GetGroupsTask().execute(userId).get();
			for(Group g: groupList) {
				groups.put(g.groupId, g);
				if(g.groupType.equals("Private")) {
					privateGroupId = g.groupId;
				}
			}
			groupList.clear();
			for (Group group: groups.values()) {
				if(group.groupType.equals("Private")) {
					groupList.add(group);
				}
			}
			for (Group group: groups.values()) {
				if(!group.groupType.equals("Private")) {
					groupList.add(group);
				}
			}
            associateColorsToGroups();
			mNavigationDrawerFragment.onNewGroups();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            List<Receipt> receiptList = new GetAllReceiptsTask().execute(userId).get();
			for(Receipt r: receiptList) {
				receipts.put(r.receiptId, r);
			}
            List<Receipt> receiptCumulusList = new GetCumulusReceipts().execute(userId, privateGroupId, 115883).get();
			Map<Integer, Receipt> receiptCumulus = new TreeMap<Integer, Receipt>();
			for(Receipt r: receiptCumulusList) {
				receiptCumulus.put(r.receiptId, r);
			}
			for(Receipt r: receiptList) {
				// Remove already transfered cumulus receipts
				if(r.sourceReceiptId != null) {
					receiptCumulus.remove(-r.sourceReceiptId);
				}
			}
			if(userId == 1) {
				receipts.putAll(receiptCumulus);
			}
        } catch (Exception e) {
            e.printStackTrace();
        }

		List<Receipt> receiptList = new ArrayList<Receipt>(receipts.values());
		Collections.sort(receiptList, new Comparator<Receipt>() {
			@Override
			public int compare(Receipt lhs, Receipt rhs) {
				return -lhs.createDate.compareTo(rhs.createDate);
			}
		});
		ListView listView = (ListView) findViewById(R.id.receiptList);
		adapter = new VisibilityFilterArrayAdapter(this, receiptList);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectReceipt(adapter.getItem(position).receiptId);
            }
        });
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
		if(adapter != null) {
			adapter.getFilter().filter(Integer.toString(position));
		}
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(getGroupName(position)))
                .commit();
    }

    public void onSectionAttached(String name) {
		mTitle = name;
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

	public void selectReceipt(int receiptId) {
		Intent intent = new Intent(this, ReceiptActivity.class);
		intent.putExtra("receiptId", receiptId);
		startActivity(intent);
	}

    public void switchToCamera(MenuItem i) {
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }

    public void associateColorsToGroups(){
        int[] colorCodes = new int[4];
        colorCodes[0] = Color.rgb(31,119,180);
        colorCodes[1] = Color.rgb(255,127,14);
        colorCodes[2] = Color.rgb(214,39,40);
        colorCodes[3] = Color.rgb(44,160,44);
		int i = 0;
        for (Group group: groupList) {
            groupColorCode.put(group.groupId, colorCodes[i % colorCodes.length]);
			i += 1;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.overview, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NAME = "section_name";

        public static PlaceholderFragment newInstance(String sectionName) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putString(ARG_SECTION_NAME, sectionName);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_overview, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((OverviewActivity) activity).onSectionAttached(getArguments().getString(ARG_SECTION_NAME));
        }
    }
}

