package ch.yeswescan.scanapp.scanapp;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;

/**
 * Created by timethy on 10/12/14
 */
public class PendingTransactionsListFragment extends Fragment {
	private static Map<Integer, List<Transaction>> transactions = new TreeMap<Integer, List<Transaction>>();
	private ViewSwitcher mViewSwitcher;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			List<Transaction> transactionList = new GetTransactionsTask().execute(LoginActivity.user.userId).get();
			for(Transaction transaction: transactionList) {
				List<Transaction> groupTransactions = transactions.get(transaction.groupId);
				if(groupTransactions == null) {
					groupTransactions = new ArrayList<Transaction>();
					transactions.put(transaction.groupId, groupTransactions);
				}
				groupTransactions.add(transaction);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_transactions, container, false);
		mViewSwitcher = (ViewSwitcher) view.findViewById(R.id.transactions_switcher);
        return view;
    }

	public void toggleSettleView() {
		if(mViewSwitcher.getDisplayedChild() != 0) {
			mViewSwitcher.showNext();
		} else {
			mViewSwitcher.reset();
		}
	}
	public void toggleTransactionView() {
		if(mViewSwitcher.getDisplayedChild() != 1) {
			mViewSwitcher.showNext();
		} else {
			mViewSwitcher.reset();
		}
	}
/*
	public void onGroupChange(Group group) {
		if(group != null) {
			List<Transaction> groupTransactions = transactions.get(group.groupId);
			ListView mListView = (ListView) mViewSwitcher.findViewById(R.id.transaction_list);
			List<Map<String, String>> transactionDisplay = new ArrayList<Map<String, String>>();
			for(Transaction transaction: groupTransactions) {
				Map<String, String> map = new TreeMap<String, String>();
				map.put("from", transaction.fromUser);
				map.put("to", transaction.toUser);
				map.put("amount", "" + transaction.amount);
				transactionDisplay.add(map);
			}
			mListView.setAdapter(new SimpleAdapter(mListView.getContext(), transactionDisplay, R.layout.fragment_transactions_item,
					new String [] { "from", "to", "amoun" }, new int [] { R.id.transaction_from, R.id.transaction_to, R.id.transaction_amount }));
		} else {

		}
	}*/
}
