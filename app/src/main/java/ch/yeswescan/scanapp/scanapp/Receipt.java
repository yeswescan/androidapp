package ch.yeswescan.scanapp.scanapp;

import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;

/**
 * Created by david on 11/10/14.
 */
public class Receipt {
    Integer receiptId;
	Integer sourceReceiptId;
    String store;
    Integer creator;
    DateTime createDate;
    DateTime uploadDate;
    String mode;
    List<Item> items;
    // Map von groupId auf subtotal einer gruppe des total
    Map<Integer, Double> groups;
    Double total;

	public String toString() {
		return createDate.toString();
	}
}
