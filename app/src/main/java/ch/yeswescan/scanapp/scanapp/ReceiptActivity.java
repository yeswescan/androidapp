//activated with an Intent, which brings an ArrayList of Strings
//those strings represent item.name, item.count and item.price in this order, for every item
//at 0 is date/time of the receipt, at 1 the location

package ch.yeswescan.scanapp.scanapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ReceiptActivity extends Activity {

    Receipt currentReceipt;
    ArrayList<Map<String,String>> stringList;
    ListView listView;

    int editing = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);

        Integer receiptId = getIntent().getIntExtra("receiptId", 0);
        currentReceipt = OverviewActivity.receipts.get(receiptId);

        getActionBar().setTitle(Util.UTIL.beautifyDate(currentReceipt.createDate) + " at " + currentReceipt.store);

        listView = (ListView) findViewById(R.id.receiptItemList);

        stringList = new ArrayList<Map<String,String>>();
        HashMap<String,String> map;

        for (Item i : currentReceipt.items) {
            map = new HashMap<String, String>();
            map.put("itemName", i.itemName);
            map.put("quantity", i.quantity.toString());
            map.put("itemPrice", Util.UTIL.beautifyMoney(i.itemPrice));
            stringList.add(map);
        }

		final SimpleAdapter table = new EditAdapter(this, stringList, R.layout.activity_receipt_item,
				new String[]{"itemName","quantity","itemPrice"}, new int[]{R.id.itemName,R.id.quantity,R.id.itemPrice} );
        listView.setAdapter(table);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.receipt, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    public void switchToOverview(MenuItem i) {
        Intent intent = new Intent(this, OverviewActivity.class);
        startActivity(intent);
    }

    private class EditAdapter extends SimpleAdapter {
		private	View.OnClickListener editButtonListener = new View.OnClickListener() {
			@Override
			public void onClick(View view) {
                editing = listView.getPositionForView(view);
                notifyDataSetChanged();
			}
		};

        private	View.OnClickListener saveButtonListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // save data
                EditText edit_itemName = (EditText) listView.getChildAt(editing).findViewById(R.id.edit_itemName);
                EditText edit_quantity = (EditText) listView.getChildAt(editing).findViewById(R.id.edit_quantity);
                EditText edit_itemPrice = (EditText) listView.getChildAt(editing).findViewById(R.id.edit_itemPrice);
                Item currentItem = currentReceipt.items.get(editing);
                currentItem.itemName = edit_itemName.getText().toString();
                currentItem.quantity = Double.parseDouble(edit_quantity.getText().toString());
                currentItem.itemPrice = Double.parseDouble(edit_itemPrice.getText().toString());
                // Save to our stringlist
                HashMap map = new HashMap<String, String>();
                map.put("itemName", currentItem.itemName);
                map.put("quantity", currentItem.quantity.toString());
                map.put("itemPrice", Util.UTIL.beautifyMoney(currentItem.itemPrice));
                stringList.set(editing, map);
                // TODO POST TO API

                // stop editing this
                editing = -1;
                notifyDataSetChanged();
            }
        };

		public EditAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
			super(context, data, resource, from, to);
		}

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
			ViewSwitcher view = (ViewSwitcher) super.getView(position, convertView, parent);
            if (editing != position) {
				if(view.getDisplayedChild() == 1) {
					view.setInAnimation(AnimationUtils.loadAnimation(view.getContext(), android.R.anim.slide_in_left));
					view.setOutAnimation(AnimationUtils.loadAnimation(view.getContext(), android.R.anim.slide_out_right));
					view.showPrevious();
				}
				view.findViewById(R.id.receipt_edit).setOnClickListener(editButtonListener);
                return view;
            } else {
				if(view.getDisplayedChild() == 0) {
					view.setInAnimation(AnimationUtils.loadAnimation(view.getContext(), R.anim.slide_in_right));
					view.setOutAnimation(AnimationUtils.loadAnimation(view.getContext(), R.anim.slide_out_left));
					view.showNext();
				}
				EditText itemNameView = (EditText) view.findViewById(R.id.edit_itemName);
				EditText itemPriceView = (EditText) view.findViewById(R.id.edit_itemPrice);
				EditText quantityView = (EditText) view.findViewById(R.id.edit_quantity);
                Item item = currentReceipt.items.get(position);
                itemNameView.setText(item.itemName);
                itemPriceView.setText(Util.UTIL.beautifyMoney(item.itemPrice));
                quantityView.setText(item.quantity.toString());
                view.findViewById(R.id.receipt_save).setOnClickListener(saveButtonListener);
                return view;
            }
        }
    }
}
