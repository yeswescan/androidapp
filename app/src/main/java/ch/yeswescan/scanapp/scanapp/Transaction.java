package ch.yeswescan.scanapp.scanapp;

import org.joda.time.DateTime;

/**
 * Created by timethy on 10/12/14
 */
public class Transaction {
	Integer groupId;
	Integer transactionId;
	String fromUser;
	String toUser;
	Double amount;
	Boolean paid;
	DateTime transcationDate;
}
