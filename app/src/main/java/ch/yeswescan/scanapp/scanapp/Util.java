package ch.yeswescan.scanapp.scanapp;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DecimalFormat;

/**
 * Created by timethy on 10/11/14
 */
public enum Util {
	UTIL;
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.shortDate();
	private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("##0.00");

	public String beautifyMoney(double money) {
		return DECIMAL_FORMAT.format(money);
	}

	public String beautifyDate(DateTime time) {
		String dateText = time.toString(DATE_FORMATTER);
		DateTime now = new DateTime();
		DateTime today = now.toLocalDate().toDateTimeAtStartOfDay();
		if(time.isAfter(today)) {
			dateText = "today";
		} else if(time.isAfter(today.minusDays(1))) {
			dateText = "yesterday";
		} else if(time.isAfter(today.minusDays(9))) {
			dateText = Days.daysBetween(time, now).getDays() + " days ago";
		}
		return dateText;
	}
}
