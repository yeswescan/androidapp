package ch.yeswescan.scanapp.scanapp;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.*;

/**
 * Created by kklein on 11/10/14.
 */
public class VisibilityFilterArrayAdapter extends ArrayAdapter<Receipt> {
	private final List<Receipt> receipts;
	private List<Receipt> displayed;

	public VisibilityFilterArrayAdapter(Context ctx, List<Receipt> receipts) {
		super(ctx, R.layout.activity_overview_item, R.id.txt_date, receipts);
		this.receipts = receipts;
		this.displayed = receipts;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Now we can fill the layout with the right values
		View view = super.getView(position, convertView, parent);
		TextView gv = (TextView) view.findViewById(R.id.txt_group);
		TextView store = (TextView) view.findViewById(R.id.txt_store);
		TextView tv = (TextView) view.findViewById(R.id.txt_date);
        TextView amount = (TextView) view.findViewById(R.id.txt_total_amount);
		Receipt r = displayed.get(position);
		Group group = OverviewActivity.groupList.get(0);
		for(Integer i: r.groups.keySet()) {
			group = OverviewActivity.groups.get(i);
			if(group != null && !group.groupType.equals("Private")) {
				break;
			}
		}
		int groupColor = OverviewActivity.groupColorCode.get(group.groupId);
		gv.setBackgroundColor(groupColor);
		gv.setText(group.groupName.substring(0,1));
		store.setTextColor(groupColor);
		store.setText(r.store);
		tv.setTextColor(groupColor);
		tv.setText(Util.UTIL.beautifyDate(r.createDate));
        amount.setText(Util.UTIL.beautifyMoney(r.total));

		return view;
	}

	public Filter getFilter() {
		return new VisibilityFilter();
	}

	private class VisibilityFilter extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			FilterResults results = new FilterResults();
			// We implement here the filter logic
			if (constraint == null || constraint.length() == 0) {
				// No filter implemented we return all the list
				results.values = receipts;
				results.count = receipts.size();
			} else {
				int id = Integer.parseInt(constraint.toString());
				if(id == 0) {
					results.values = new ArrayList<Receipt>(receipts);
					results.count = receipts.size();
				} else if (OverviewActivity.groupList.size() + 1 == id) {
					// We are in Cumulus
					List<Receipt> nReceiptList = new ArrayList<Receipt>();

					for (Receipt r : receipts) {
						if(r.receiptId < 0 && r.sourceReceiptId != null) {
							nReceiptList.add(r);
						}
					}

					results.values = nReceiptList;
					results.count = nReceiptList.size();
				} else {
					List<Receipt> nReceiptList = new ArrayList<Receipt>();

					for (Receipt r : receipts) {
						if(r.receiptId > 0) {
							for(Item i: r.items) {
								if(i.groupId == id) {
									nReceiptList.add(r);
									break;
								}
							}
						}
					}

					results.values = nReceiptList;
					results.count = nReceiptList.size();
				}
			}
			return results;
		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			displayed = (List<Receipt>) results.values;
			clear();
			addAll(displayed);
			notifyDataSetChanged();
		}
	}
}
